# U.S. Web Design Standards – Drupal 8 theme

Drupal Integration of the [U.S. Web Design Standards](https://standards.usa.gov/) library.

The library is included in the 'assets' directory.


## Theme Configuration
After installing the theme, go to Theme Settings (admin/appearance > Click on "Settings" for this theme) there you will be able to change:

1. Color scheme (using color module) >> TO-DO
2. Header and Footer styles

## Notes

The code for this theme is based in [web design standards theme](https://github.com/18F/web-design-standards-drupal) from 18F team, and was customized to fit with our requeriments.
