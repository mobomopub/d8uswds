<?php

/**
 * @file
 * Alterations for this form.
 */

use \Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function uswds_form_search_block_form_alter(&$form, FormStateInterface $form_state, $form_id ) {
  //if is a node, init $node vars. We need these to be able to check theme settings per CT
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $node_type = $node->getType();
    $header_type = theme_get_setting('uswds_header_' . $node_type);
  // if not a node then get page header setting
  } else {
    $header_type = theme_get_setting('uswds_pages_header_style');
  }

  $form['#attributes']['class'][] = 'usa-search';
  $form['#attributes']['class'][] = 'usa-search-small';

  // No need to control the size.
  unset($form['keys']['#size']);

  // Add the role of search.
  $form['keys']['#prefix'] = '<div role="search">';
  $form['actions']['#suffix'] = '</div>';

  // Add javascript classes if this is the extended header.
  if ($header_type == 'extended') {
      $form['#attributes']['class'][] = 'js-search-form';
      $form['#attributes']['class'][] = 'usa-sr-only';
  }

  // Remove the "value" so that the search button is only the icon, but hack
  // around the submit button for accessibility reasons.
  $form['actions']['submit']['#value'] = '';
  $form['actions']['submit']['#prefix'] = '<button type="submit"><span class="usa-sr-only">Search</span>';
  $form['actions']['submit']['#suffix'] = '</button>';
}
