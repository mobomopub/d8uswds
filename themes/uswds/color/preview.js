/**
 * @file
 * Preview for the Bartik theme.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.color = {
    logoChanged: false,
    callback: function (context, settings, $form) {
      // Change the logo to be the real one.
      if (!this.logoChanged) {
        $('.color-preview .color-preview-logo img').attr('src', drupalSettings.color.logo);
        this.logoChanged = true;
      }
      // Remove the logo if the setting is toggled off.
      if (drupalSettings.color.logo === null) {
        $('div').remove('.color-preview-logo');
      }

      var $colorPreview = $form.find('.color-preview');
      var $colorPalette = $form.find('.js-color-palette');

      // Header Background
      $colorPreview.find('.usa-header').css('backgroundColor', $colorPalette.find('input[name="palette[usa-navbar-bg]"]').val());

      // USA Logo.
      $colorPreview.find('.usa-logo .usa-logo-text a').css('color', $colorPalette.find('input[name="palette[usa-logo-txt]"]').val());
      // USA Nav primary links.
      $colorPreview.find('.usa-nav-primary button, .usa-nav-primary > li > a').css('color', $colorPalette.find('input[name="palette[usa-nav-primary-links]"]').val());
      // USA Nav secondary links.
      $colorPreview.find('.usa-nav-secondary-links a, .usa-nav-secondary-links .usa-header-search-button').css('color', $colorPalette.find('input[name="palette[usa-nav-secondary-links]"]').val());
      // USA Nav bg colour.
      $colorPreview.find('.usa-nav').css('backgroundColor', $colorPalette.find('input[name="palette[usa-nav-bg]"]').val());
      // Tagline Heading.
      $colorPreview.find('.usa-section .usa-width-one-third h2').css('color', $colorPalette.find('input[name="palette[usa-tagline-heading]"]').val());
      // Tagline body.
      $colorPreview.find('.usa-section .usa-width-two-thirds p').css('color', $colorPalette.find('input[name="palette[usa-tagline-body]"]').val());
      // USA graphic list body.
      $colorPreview.find('.usa-graphic_list p').css('color', $colorPalette.find('input[name="palette[usa-graphic-list-p]"]').val());
      // USA graphic list bg colour.
      $colorPreview.find('.usa-graphic_list').css('backgroundColor', $colorPalette.find('input[name="palette[usa-graphic-list-bg]"]').val());
      // USA graphic list headings.
      $colorPreview.find('.usa-graphic_list h3').css('color', $colorPalette.find('input[name="palette[usa-graphic-list-headings]"]').val());
      // USA Footer Primary bg colour.
      $colorPreview.find('.usa-footer-primary-section').css('backgroundColor', $colorPalette.find('input[name="palette[usa-footer-primary-section]"]').val());
      // USA Footer Primary links.
      $colorPreview.find('.usa-footer .usa-footer-primary-link').css('color', $colorPalette.find('input[name="palette[usa-footer-primary-link]"]').val())
      // USA Footer Secondary bg colour.
      $colorPreview.find('.usa-footer-secondary_section').css('backgroundColor', $colorPalette.find('input[name="palette[usa-footer-secondary_section]"]').val());
    }
  };
})(jQuery, Drupal, drupalSettings);
