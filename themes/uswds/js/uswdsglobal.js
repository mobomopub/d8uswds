(function ($, Drupal) {

  'use strict';

  /**
   * Example function.
   * @see https://www.drupal.org/node/304258#drupal-behaviors
   */
  Drupal.behaviors.theme = {
    attach: function (context, settings) {

            var section = $("section[class='usa-section']"),
                cklink = section.find($('p a'));
            cklink.addClass( "usa-button usa-button-big" );

            var div = $("div[class='font-lead']"),
                fontlead = div.find($('div p'));
            fontlead.addClass( "usa-font-lead" );

    }
  };
})(jQuery, Drupal);





