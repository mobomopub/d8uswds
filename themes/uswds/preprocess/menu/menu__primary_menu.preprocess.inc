<?php

/**
 * @file
 * Preprocess function for this hook.
 */

/**
 * Implements hook_preprocess_menu__REGION().
 *
 * This made possible by our custom theme suggestion.
 * @see uswds_theme_suggestions_menu_alter().
 */
function uswds_preprocess_menu__primary_menu(&$variables) {
  //if is a node, init $node vars. We need these to be able to check theme settings per CT
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $node_type = $node->getType();
    $header_type = theme_get_setting('uswds_header_' . $node_type);
  // if not a node then get page header setting
  } else {
    $header_type = theme_get_setting('uswds_pages_header_style');
  }

  $variables['megamenu'] = theme_get_setting('uswds_header_mega');

  if (\Drupal::moduleHandler()->moduleExists('search') && theme_get_setting('uswds_search')) {
    // If this is a basic header, we put the search form after the menu.
      if ($header_type == 'basic') {
        $search_form = \Drupal::formBuilder()->getForm('\Drupal\search\Form\SearchBlockForm');
        $variables['search_form'] = $search_form;
      }
  }
}
